package model.data_structures;

import java.util.Iterator;
import java.util.List;

import model.data_structures.ListaDobleEncadenada.NodoDoble;


public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primero;
	private NodoSencillo<T> actual;
	private int posicion;
	private int tama�o;
	
	public ListaEncadenada()
	{
		primero = null;
		actual = primero;
		tama�o = 0;
		int posicion= 0;
	}

	@Override
	public Iterator<T> iterator() {

		return new Iterator<T>(){

			NodoSencillo<T> siguiente = primero;

			public boolean hasNext(){

				if(primero == null){return false;}
				if(primero.darSiguiente() == null){return false;}
				else{return true;}
			};

			public T next()
			{
				T item = null;
				if(primero != null)
				{item = (T) primero.darSiguiente().item;
				siguiente = primero.darSiguiente(); 
				}
				return item;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			};

		};
	}

	@Override
	public void agregarElementoFinal(T elem) {

		if( primero == null )
		{	primero = new NodoSencillo<T>(elem);
		    actual = primero;
		tama�o++;
		}
		else
		{
			NodoSencillo<T> antiguoUltimo = localizarUltimo();
			NodoSencillo<T> ultimo = new NodoSencillo<T>(elem);
			antiguoUltimo.siguiente = ultimo;
			tama�o++;
		}
	}


	@Override
	public T darElemento(int pos) {

		int contador = 0;

		NodoSencillo<T> actual = primero;

		while(actual.darSiguiente() != null && contador != pos)
		{
			actual = actual.darSiguiente();
			contador++;
		}

		return actual.darItem();
	}


	@Override
	public int darNumeroElementos() {

		return tama�o;
	}

	@Override
	public T darElementoPosicionActual() {
		return actual.darItem();
	}

	@Override

	public boolean avanzarSiguientePosicion() {
		
		boolean avanza= false;
		NodoSencillo<T> actual = primero;
		if(!darElementoPosicionActual().equals(localizarUltimo()))
		{
			avanza = true;
			posicion++;
			actual = (NodoSencillo<T>) darElemento(posicion);
		}
		return avanza;
		
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		
		boolean retrocede= false;
		NodoSencillo<T> actual= primero;
		if(!darElementoPosicionActual().equals(primero))
		{
			retrocede = true;
			posicion--;
			actual = (NodoSencillo<T>) darElemento(posicion);
		}
		return retrocede;
	}


	/**
	 * Clase que representa el nodo de la lista encadenada
	 */
	public class NodoSencillo<T>
	{

		private NodoSencillo<T> siguiente; 
		private T item;

		public NodoSencillo(T pItem)
		{
			siguiente = null;
			item = pItem;
		}

		public NodoSencillo<T> darSiguiente()
		{
			return  siguiente;
		}

		public T darItem()
		{
			return item;
		}
	
	}

	public NodoSencillo<T> localizarUltimo()
	{
		NodoSencillo<T> actual = primero;

		while( actual != null && actual.darSiguiente(  ) != null )
		{
			actual = actual.darSiguiente(  );
		}
		return actual;
	}
	
<<<<<<< HEAD
	public void reiniciarLista()
	{
		primero = null;
=======
	public void verificarInvariante()
	{
		assert(darNumeroElementos() != 0): "La lista no puede estar vac�a";
>>>>>>> b172c4168fd5d9245430ca39220e8f1b54bd9de4
	}

}






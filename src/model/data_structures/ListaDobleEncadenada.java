package model.data_structures;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.ListaEncadenada.NodoSencillo;

public class ListaDobleEncadenada<T> implements ILista<T>
{

	private NodoDoble<T> primerElemento;
	private NodoDoble<T> ultimoElemento;
	private NodoDoble<T> actual;
	private int posicion;
	private int tamanio;

	
	
	public ListaDobleEncadenada() 
	{
		primerElemento = null;
	    actual = primerElemento;
	    ultimoElemento = null;
	    tamanio = 0;
	    posicion = 0;
	    
	    verificarInvariante();
	}
	
	
	/**
	 * Clase que representa el nodo doble implementados para la lista
	 */
	public class NodoDoble<T>
	{		
		private NodoDoble<T> siguiente; 
		private NodoDoble<T> anterior;
		private T item;

		public NodoDoble(T pItem)
		{
			siguiente = null;
			anterior= null;
			item= pItem;
		}

		public T darItem()
		{
			return item;
		}

		public NodoDoble<T> darSiguiente()
		{
			return  siguiente;
		}

		public NodoDoble<T> darAnterior()
		{
			return anterior;
		}
	}


	public Iterator<T> iterator() 
	{	
		return new Iterator<T>(){

			NodoDoble<T> siguiente = primerElemento;

			public boolean hasNext(){

				if(primerElemento == null) return false;
				if(primerElemento.darSiguiente() == null) return false;
				else
					return true;
			};

			public T next()
			{
				T item = null;
				if(primerElemento != null)
				{
					item = primerElemento.darSiguiente().item;
					siguiente = primerElemento.darSiguiente();
				}
				return item;
			}

			public void remove() {
				// M�todo no implementado en el programa				
			};

		};
	}


	public void agregarElementoFinal(T elem) {

		if(primerElemento == null)
		{
			primerElemento = new NodoDoble<T>(elem);
			actual= primerElemento;
			tamanio++;
		}
		else
		{
			NodoDoble<T> ultimo= localizarUltimo();
			NodoDoble<T> nuevoUltimo= new NodoDoble<T>(elem);
			ultimo.siguiente= nuevoUltimo;
			nuevoUltimo.anterior= ultimo;
			tamanio++;
		}
				
	}

	public NodoDoble<T> localizarUltimo()
	{
		NodoDoble<T> actual= primerElemento;

		if(actual!= null)
		{
			while(actual.darSiguiente()!= null)
			{
				actual= actual.darSiguiente();
			}
		}
		return actual;
	}

	@Override
	public T darElemento(int pos) {

		int posicionActual = 0;

		NodoDoble<T> actual = primerElemento;

		while(actual.darSiguiente()!= null && posicionActual!= pos)
		{
			actual = actual.darSiguiente();
			posicionActual++;
		}

		return actual.darItem();
	}


	@Override
	public int darNumeroElementos() {
		return tamanio;
	}

	@Override
	public T darElementoPosicionActual() {
		return actual.darItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		boolean avanza= false;
		NodoDoble<T> actual= (NodoDoble<T>) darElementoPosicionActual();
		if(!actual.equals(localizarUltimo()))
		{
			avanza= true;
			actual= actual.darSiguiente();
		}
		return avanza;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		boolean retrocede= false;
		NodoDoble<T> actual= (NodoDoble<T>) darElementoPosicionActual();
		if(!darElementoPosicionActual().equals(primerElemento))
		{
			retrocede= true;
			actual= actual.darAnterior();
		}
		return retrocede;
	}
	
	public void verificarInvariante()
	{
		assert(darNumeroElementos() != 0): "La lista no puede estar vac�a";
	}

}



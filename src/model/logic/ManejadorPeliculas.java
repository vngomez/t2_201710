package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.csvreader.CsvReader;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {

<<<<<<< HEAD
		String infoMovie = "";
		int agno = 0;
		String readString = "";
=======
<<<<<<< HEAD
=======
	
		try{
		File archivo = new File(archivoPeliculas);
		FileReader fileReader = new FileReader(archivo);
		CsvReader csvReader = new CsvReader(fileReader,',');
>>>>>>> 2204a6e72a05b9f98b4658e3e217e0b5c898cf5a
>>>>>>> b172c4168fd5d9245430ca39220e8f1b54bd9de4

		ListaEncadenada<String> generosPeliculas = new ListaEncadenada<String>();
		misPeliculas = new ListaEncadenada<VOPelicula>();
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>(); 


		try{

			CsvReader csvReader = new CsvReader(archivoPeliculas);
			csvReader.readHeaders();

			while(csvReader.readRecord())
			{ 
				String movieId = csvReader.get("movieId");
				String title = csvReader.get("title");
				String genres = csvReader.get("genres");

				StringTokenizer divisorCadena = new StringTokenizer(title, "()", false);
				infoMovie = divisorCadena.nextElement().toString();

				while(divisorCadena.hasMoreTokens())
				{
					readString = divisorCadena.nextToken();

					try{
						agno = Integer.parseInt(readString);
					}
					catch(NumberFormatException e)
					{
						if(!infoMovie.equals(readString))
						{
							infoMovie = infoMovie.concat("("+readString+")");
							//Esto se usa para el caso particular de la pel�cula 265
						}
					}
				}



				String[] listaGenero = genres.split("\\|");
                  
				for(int i = 0; i<listaGenero.length; i++)
				{
					generosPeliculas.agregarElementoFinal(listaGenero[i]);
				}
              
				VOPelicula nueva = new VOPelicula();
				
				nueva.setTitulo(infoMovie);
				nueva.setAgnoPublicacion(agno);
				nueva.setGenerosAsociados(generosPeliculas);
				
				misPeliculas.agregarElementoFinal(nueva);
				
				generosPeliculas.reiniciarLista();
			}
			
			csvReader.close();
		}
		
		catch(Exception e)
		{

		}
		}
		catch(Exception e)
		{
			
		}
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {

		ArrayList<VOPelicula> respuesta = new ArrayList<VOPelicula>( );
		VOPelicula primera = misPeliculas.darElemento(0);

		while( primera != null )
		{
			if( primera.getTitulo().toLowerCase().contains(busqueda.toLowerCase()) )
			{
				respuesta.add(primera);

			}
			primera = respuesta.iterator().next();
		}

		return (ILista<VOPelicula>) respuesta;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		ArrayList<VOPelicula> lista = new ArrayList<VOPelicula>( );
		VOPelicula primera = misPeliculas.darElemento(0);

		while( primera != null )
		{
			if( primera.getAgnoPublicacion()==agno )
			{
				lista.add(primera);
			}

			primera = lista.iterator().next();
		}

		return (ILista<VOPelicula>) lista;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		return null;
	}

}

package t2_201710;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class ListaDobleEncadenadaTest<T> extends TestCase
{
	private ListaDobleEncadenada<T> lista;


	
	public void setupEscenario1()
	{
		lista= new ListaDobleEncadenada<T>();
		
	}
	
	public void setupEscenario2()
	{
		try
		{
			lista = new ListaDobleEncadenada<T>();

			T primero = (T) "Pelicula1";
			T segundo = (T) "Pelicula2";
			T tercero =  (T) "Pelicula3";


			lista.agregarElementoFinal(primero);
			lista.agregarElementoFinal(segundo);
			lista.agregarElementoFinal(tercero);


		}
		catch( Exception e )
		{
			fail("No deber�a lanzar excepci�n.");
		}

	}

	public void testListaDoble( )
	{
		setupEscenario1();
		assertEquals( "La lista debe encontrarse vac�a.", 0, lista.darNumeroElementos() );
		
		setupEscenario2( );

		assertEquals( "La lista no se encuentra vac�a.", 3, lista.darNumeroElementos() );
		assertNotNull( "La lista deber�a tener un primer elemento.", lista.darElemento(0));
		assertEquals( "El primer elemento no es el esperado.", "Pelicula1", lista.darElemento(0));
		assertEquals( "El segundo elemento no es el esperado.", "Pelicula2", lista.darElemento(1));
		assertEquals( "El ultimo elemento no es el esperado.", "Pelicula3", lista.localizarUltimo().darItem());

	}

	public void testDarElementoPosicionActual()
	{		
		setupEscenario2();
		assertEquals( "El elemento no es el de la posicion actual", "Pelicula1", lista.darElementoPosicionActual() );
	}

	public void testProbarDarElemento()
	{
		setupEscenario2();
		assertEquals("El elemento no es el esperado", "Pelicula2", lista.darElemento(1));
	}

	public void testProbarDarNumeroElementos()
	{
		setupEscenario1();
		assertEquals("La lista se encuentra vacia", 0, lista.darNumeroElementos());
		
		setupEscenario2();
		assertEquals("El tama�o no es el esperado", 3, lista.darNumeroElementos());
	}

	public void testprobarAvanzar()
	{
		setupEscenario2();
		if(lista.darElementoPosicionActual().equals(lista.localizarUltimo()))
			assertFalse("No se puede avanzar porque es el ultimo elemento", lista.avanzarSiguientePosicion());

	}

	public void testprobarRetroceder()
	{
		setupEscenario2();
		if(lista.darElementoPosicionActual().equals(lista.localizarUltimo()))
			assertFalse("No se puede retroceder porque es el primer elemento", lista.retrocederPosicionAnterior());

	}
	
	public void testLocalizarUltimo()
	{
		setupEscenario2();
		assertEquals("El ultimo no es el esperado", "Pelicula3", lista.localizarUltimo().darItem());
	}
}

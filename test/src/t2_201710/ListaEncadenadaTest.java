package t2_201710;

import model.data_structures.ListaEncadenada;
import junit.framework.TestCase;

<<<<<<< HEAD
public class ListaEncadenadaTest<T> extends TestCase
{
	private ListaEncadenada<T> listaEncadenada;

	private void setupEscenario1()
	{
		listaEncadenada= new ListaEncadenada<T>();
	}
	
	private void setupEscenario2()
	{
		try
		{
			listaEncadenada = new ListaEncadenada<>();

			T primerElemento = (T) "Movie1";
			T segundoElemento = (T) "Movie2";
			T tercerElemento = (T) "Movie3";


			listaEncadenada.agregarElementoFinal(primerElemento);
			listaEncadenada.agregarElementoFinal(segundoElemento);
			listaEncadenada.agregarElementoFinal(tercerElemento);


		}
		catch( Exception e )
		{
			fail("No deber�a lanzar excepci�n.");
		}

	}

	public void testListaEncadenada( )
	{
		setupEscenario1();
		assertEquals("La lista debe encontrarse vac�a.", 0, listaEncadenada.darNumeroElementos());
		
		setupEscenario2( );

		assertEquals( "La lista  debe crearse vac�a.", 3, listaEncadenada.darNumeroElementos() );
		assertNotNull( "La lista deber�a tener un primer elemento.", listaEncadenada.darElemento(0));
		assertEquals( "El primer elemento no es el esperado.", "Movie1", listaEncadenada.darElemento(0));
		assertEquals( "El segundo elemento no es el esperado.", "Movie2", listaEncadenada.darElemento(1));
		assertEquals( "El ultimo elemento no es el esperado.", "Movie3", listaEncadenada.localizarUltimo().darItem());

	}

	public void testDarElementoPosicionActual()
	{
		setupEscenario2();

		assertEquals( "La lista  debe crearse vac�a.", "Movie1", listaEncadenada.darElementoPosicionActual() );

	}
	public void testProbarDarElemento()
	{
		setupEscenario2();
		assertEquals("El elemento no es el esperado", "Movie2", listaEncadenada.darElemento(1));
	}

	public void testProbarDarNumeroElementos()
	{
		setupEscenario1();
		assertEquals("La lista debe encontrarse vac�a.", 0, listaEncadenada.darNumeroElementos());
		
<<<<<<< HEAD
		setupEscenario2();
		assertEquals("El tama�o no es el esperado", 3, listaEncadenada.darNumeroElementos());
	}

	public void testprobarAvanzar()
	{
		setupEscenario2();
		if(listaEncadenada.darElementoPosicionActual().equals(listaEncadenada.localizarUltimo()))
			assertFalse("No se puede avanzar porque es el ultimo elemento", listaEncadenada.avanzarSiguientePosicion());

	}

	public void testprobarRetroceder()
	{
		setupEscenario2();
		if(listaEncadenada.darElementoPosicionActual().equals(listaEncadenada.localizarUltimo()))
			assertFalse("No se puede retroceder porque es el primer elemento", listaEncadenada.retrocederPosicionAnterior());

=======
        assertEquals( "La lista  debe crearse vac�a.", "Movie1", listaEncadenada.darElementoPosicionActual() );
       
=======
public class ListaEncadenadaTest extends TestCase
{
	private ListaEncadenada listaEncadenada;
	
	private void setupExcenatio1()
	{
		listaEncadenada = new ListaEncadenada<>();
		
		
>>>>>>> 2204a6e72a05b9f98b4658e3e217e0b5c898cf5a
>>>>>>> b172c4168fd5d9245430ca39220e8f1b54bd9de4
	}
	

	
	public void testLocalizarUltimo()
	{
		setupEscenario2();
		assertEquals("El ultimo no es el esperado", "Movie3", listaEncadenada.localizarUltimo().darItem());
	}
}
